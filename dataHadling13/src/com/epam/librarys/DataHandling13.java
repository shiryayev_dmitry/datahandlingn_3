package com.epam.librarys;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataHandling13 {

    public static void main(String[] args) {
	// write your code here
        String str = "+7(3412)453-923";
        Pattern pattern = Pattern.compile("\\d*-\\d*-?\\d*");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            System.out.println(matcher.group(0));
        }
    }
}
